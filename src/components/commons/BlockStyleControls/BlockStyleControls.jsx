import React from 'react';
import { observer } from 'mobx-react';
import { INLINE_BLOCK_TYPES, BLOCK_TYPES } from '../../../consts';

import { draftjsStore } from '../../../mobx/draftjsStore';


export const BlockStyleControls = observer(({ onChangeHandler }) => {

  const selection = draftjsStore.editorState.getSelection();

  const blockType = draftjsStore.editorState
    .getCurrentContent()
    .getBlockForKey(selection.getStartKey())
    .getType();

  const toggleInlineHandler = (style) => {
    onChangeHandler(draftjsStore.toggleInlineStyle(draftjsStore.editorState, style));
  }

  const toggleBlockHandler = (style) => {
    onChangeHandler(draftjsStore.toggleBlockStyle(draftjsStore.editorState, style));
  }

  return (
    <>
        {
            INLINE_BLOCK_TYPES.map(({label, style}) => {
                return <button 
                    active={style === blockType}
                    key={label}
                    onClick={() => toggleInlineHandler(style)}
                >{label}</button>
            })
        }
        {
            BLOCK_TYPES.map(({label, style}) => {
                return <button 
                    active={style === blockType}
                    key={label}
                    onClick={() => toggleBlockHandler(style)}
                >{label}</button>
            })
        }
    </>
  )
})
