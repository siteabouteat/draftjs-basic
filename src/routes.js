import React from 'react';
import { Routes, Route } from 'react-router-dom';

import {Home, BaseDraft} from "./pages";

export const AppRoutes = () => {
    return (
        <>
            <Routes>
                <Route path="/" isHome element={
                    <>
                        <Home />
                    </>
                } />
                <Route path="/base-draft" isBaseDraft element={
                    <>
                        <BaseDraft />
                    </>
                } />
            </Routes>
        </>
    );
};