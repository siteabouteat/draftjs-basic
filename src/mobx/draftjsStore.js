import {makeAutoObservable, runInAction, toJS} from "mobx";

import {EditorState, RichUtils} from "draft-js";

class Draftjs {

    constructor(){
        makeAutoObservable(this);
    }

    // createWithContent - если нужен какойто контент
    editorState = EditorState.createEmpty();

    onChange = (editorState) => {
        this.editorState = editorState;
        console.log('onChangeHandler', this.editorState.toJS());
    };
    
    // RichUtils данная утилита позволяет просто работать с базовыми возможностями
    toggleInlineStyle = (editorState, style) => {
        return RichUtils.toggleInlineStyle(editorState, style);
    }    
    
    toggleBlockStyle = (editorState, style) => {
        return RichUtils.toggleBlockType(editorState, style);
    }
}

export const draftjsStore = new Draftjs();