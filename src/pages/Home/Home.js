import {NavLink} from "react-router-dom";

export const Home = () => {
    return (
        <div className="Home">
            <div><NavLink to="/base-draft">Base draft</NavLink></div>
        </div>
    );
};