import { CompositeDecorator, ContentBlock, ContentState, EditorState, SelectionState, convertToRaw } from 'draft-js';

export const MyLink = (props) => {
    console.log('props: ', props);
    const { contentState, entityKey, children, block } = props;
    // const { content, selection } = getGeneralPropertiesFromEditorState(contentState);

    const { url } = contentState.getEntity(entityKey).getData();


    const goToNandler = (e) => {
        e.preventDefault();
        // window.location.href = `http://${url}`;
        console.log('URL redirect piu piu', url);
    };

    console.log('URL in decorator component: ', url);

    const rejectUrl = url === "ya.ru" ? 'reject' : null;

    const onDragStartHandler = (ev) => {
        // ev.stopPropagation();
        // ev.preventDefault();
        ev.dataTransfer.dropEffect = 'move';
        console.log('dragStart', ev.target.innerText);
        console.log('dragStart block ==> ', block);
        // ev.dataTransfer.setData("text/html", ev.target.outerHTML);
        ev.dataTransfer.setData(
            'text',
            `${'DRAFTJS_BLOCK_KEY'}:${entityKey}`
        );
        console.log('contentState', contentState.toJS());
        // contentState.draggable = true;
        // contentState.getCurrentContent();
        // const contentWithRemovedLink = Modifier.applyEntity(contentState);
        // const newEditorState = EditorState.set(
        //     contentState, { currentContent: contentWithRemovedLink });
    }

    const onDropHandler = (ev) => {
        // ev.stopPropagation();
        // ev.preventDefault();
        console.log('dragDrop', ev.target.innerText);
        ev.dataTransfer.setData("text/html", ev.target.outerHTML);
        console.log('dragDrop contentState', contentState.toJS());
        // contentState.draggable = true;
        // contentState.getCurrentContent();
        // const contentWithRemovedLink = Modifier.applyEntity(contentState);
        // const newEditorState = EditorState.set(
        //     contentState, { currentContent: contentWithRemovedLink });
    }

    const onMouseDownHandler = (e) => {
        console.log('e: ', e.target);
    };

    return (
        <a
            href={url}
            contentEditable={false}
            //onDrop={onDropHandler}
            onDragStart={onDragStartHandler}
            draggable="true"
            style={{paddingLeft: '10px'}}
            className={rejectUrl}
            // onClick={goToNandler}
        >
            { children }
        </a>
        // <a href={url} onMouseDown={onMouseDownHandler} draggable="true" style={{paddingLeft: '10px'}} className={rejectUrl} onClick={goToNandler}>
        //     { children }
        // </a>
    );
};

export const findLinkEntities = (contentBlock, callback, contentState) => {

    const filterFn = character => {
        const entityKey = character.getEntity();
        return (
            entityKey !== null &&
            contentState.getEntity(entityKey).getType() === 'LINK'
        )
    };

    contentBlock.findEntityRanges(filterFn, callback)
}

export const decorator = new CompositeDecorator([
    {
        strategy: findLinkEntities,
        component: MyLink,
    },
]);


export const createNewEditorState = (editorState, params) => {
    const {
        newText = editorState.getCurrentContent().getFirstBlock().getText(),
        focus = editorState.getSelection().getFocusOffset(),
        blockKey = editorState.getCurrentContent().getFirstBlock().getKey(),
        isClearSpace = false,
    } = params;


    const newBlockMap = isClearSpace ?
        (editorState.getCurrentContent().getBlockMap().slice(0, 1).map((block) => {
            return new ContentBlock({
                text: newText,
                key: blockKey,
                type: 'unstyled',
            });
        })) :
        (editorState.getCurrentContent().getBlockMap().map((block) => {
            return new ContentBlock({
                text: newText,
                key: block.getKey(),
                type: block.getType(),
                characterList: block.getCharacterList(),
            });
        }));


    const newSelectionState = new SelectionState({
        anchorKey: blockKey,
        anchorOffset: focus,
        focusKey: blockKey,
        focusOffset: focus,
    });

    const newContentState = new ContentState({
        blockMap: newBlockMap,
    });

    const newEditorState = EditorState.createWithContent(newContentState, decorator);
    return EditorState.forceSelection(newEditorState, newSelectionState);
};

/**
 * Функция Вставляет текст в EditorState
 * @param editorState{EditorState} - Обьект EditorState
 * @param text{string} - Вставляемый текст
 * @param maxLength{number} - Длинна обрезанного текста
 * @returns {EditorState} - Обьект EditorState с удаленными лишними пробелами
 * и установленным курсором в месте предыдущего состояния и вставленным текстом
 */
export const copyPasteText = (text = '', editorState, maxLength) => {
    // /((([A-Za-z]{3,9}:(?:\/\/)?)(?:[\-;:&=\+\$,\w]+@)?[A-Za-z0-9\.\-]+|(?:www\.|[\-;:&=\+\$,\w]+@)[A-Za-z0-9\.\-]+)((?:\/[\+~%\/\.\w\-_]*)?\??(?:[\-\+=&;%@\.\w_]*)#?(?:[\.\!\/\\\w]*))?)/
    const start = editorState.getSelection().getStartOffset();
    const end = editorState.getSelection().getEndOffset();
    const  isBackWard= editorState.getSelection().getIsBackward();
    //console.log('isBackWard', isBackWard);
    let currentText = editorState.getCurrentContent().getPlainText();
    console.log('currentText', currentText);
    const blocks = convertToRaw(editorState.getCurrentContent()).blocks;
    const value = blocks.map(block => (!block.text.trim() && '\n') || block.text).join('\n');
    console.log('allText', value);
    let newFocus = editorState.getSelection().getFocusOffset() + (text?.length || 0) + 1;// 2 -это добавленные пробелы
    if (currentText.length === 0) {
        newFocus -= 1;
    }
    if (start !== end) {
        currentText = currentText.slice(0, start) + currentText.slice(end, currentText.length);
        newFocus -= (end - start);
    }
    const focusPaste = isBackWard ?
        editorState.getSelection().getFocusOffset() :
        editorState.getSelection().getFocusOffset() - (end - start);

    let newText = `${currentText.slice(0, focusPaste)} ${text} ${currentText.slice(focusPaste, currentText.length)}`;
    if (newText.length > maxLength) {
        newText = newText.slice(0, maxLength);
        newFocus = maxLength;
    }
    // Убираем все символы \n и лишние пробелы
    newText = newText.replace(/\n/gi, ' ').replace(/ {1,}/g, ' ').trim();
    return createNewEditorState(editorState, { focus: newFocus, newText, isClearSpace: true });
};