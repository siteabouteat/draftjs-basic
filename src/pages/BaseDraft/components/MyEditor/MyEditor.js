import {useEffect, useRef, useState} from "react";
import { observer } from 'mobx-react';
import {Editor, EditorState, RichUtils} from "draft-js";
import { draftjsStore } from "../../../../mobx/draftjsStore";
import { getSelectionRange, getSelectionCoords } from "../../../../utils";
import {copyPasteText} from "./helpers";

import { BlockStyleControls } from "../../../../components";

export const MyEditor = observer(() => {

    const editor = useRef(null);

    function focusEditor() {
        editor.current.focus();
    }

    useEffect(() => {
        focusEditor()
    }, []);

    return (
        <div
            id="editor-container"
            className="c-editor-container js-editor-container"
        >
            <div className="editor">
                <BlockStyleControls onChangeHandler={draftjsStore.onChange} />
                <Editor
                    ref={editor}
                    editorState={draftjsStore.editorState}
                    onChange={draftjsStore.onChange}
                    // handlePastedText={copyPasteHandler}
                    // handleKeyCommand={handleKeyCommand}
                    placeholder="Здесь можно печатать..."
                />
            </div>
        </div>
    );
});