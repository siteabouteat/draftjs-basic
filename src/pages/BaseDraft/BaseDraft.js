import {MyEditor} from "./components/MyEditor/MyEditor";
import 'draft-js/dist/Draft.css';

export const BaseDraft = () => {
    return (
        <div className="BaseDraft">
            <MyEditor />
        </div>
    );
};